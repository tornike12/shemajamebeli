package com.example.korinteli

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    private val db: DatabaseReference = FirebaseDatabase.getInstance().reference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val name = dataSnapshot.child(emailIn.text.toString()).child("name").value.toString()
                val gvari = dataSnapshot.child(emailIn.text.toString()).child("gvari").value.toString()
                val asaki = dataSnapshot.child(emailIn.text.toString()).child("age").value.toString()
                val nomeri = dataSnapshot.child(emailIn.text.toString()).child("nomeri").value.toString()
                nameai.setText(name)
                gvariai.setText(gvari)
                asakii.setText(asaki)
                nomerai.setText(nomeri)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("loadPost:onCancelled", databaseError.toException())
                // ...
            }
        })
    }
}
