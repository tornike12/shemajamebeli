package com.example.korinteli

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : AppCompatActivity() {
    private val db: DatabaseReference = FirebaseDatabase.getInstance().reference
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        val hashMap = HashMap<String, String>()
        auth = FirebaseAuth.getInstance()
        signUp.setOnClickListener {
            hashMap.put("E-mail",emailUp.text.toString())
            hashMap.put("password",passwordUp.text.toString())
            hashMap.put("name", saxeli.text.toString())
            hashMap.put("age", age.text.toString())
            hashMap.put("gvari",gvari.text.toString())
            hashMap.put("nomeri",mobiluri.text.toString())
            db.child(emailUp.text.toString()).setValue(hashMap)
            auth.createUserWithEmailAndPassword(
                emailUp.text.toString(),
                passwordUp.text.toString()
            )
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                }



        }
    }
}
