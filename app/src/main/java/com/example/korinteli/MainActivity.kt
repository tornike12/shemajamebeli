package com.example.korinteli

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        readData()
        registration.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
        auth = FirebaseAuth.getInstance()
        signIn.setOnClickListener {
            saveUserData()
            intent.putExtra("email",emailIn.text.toString())
            intent.putExtra("pass",passwordIn.text.toString())
            auth.signInWithEmailAndPassword(emailIn.text.toString(), passwordIn.text.toString())
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Toast.makeText(this, "succes", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this, "arasworia", Toast.LENGTH_LONG).show()
                    }

                }

        }
    }
        private fun saveUserData(){
            val sharedPreferences = getSharedPreferences("user_Data", Context.MODE_PRIVATE)
            val edit = sharedPreferences.edit()
            edit.putString("email",emailIn.text.toString())
            edit.putString("pass",passwordIn.text.toString())
        }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_Data",Context.MODE_PRIVATE)
        emailIn.setText(sharedPreferences.getString("email",""))
        passwordIn.setText(sharedPreferences.getString("pass",""))
    }

}
